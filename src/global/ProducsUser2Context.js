import React, { createContext } from 'react';
import movedService from '../service/movedService';
// import { withFirebase } from '../components/Firebase';
// import { db } from '../config/Config';
import { dbstore } from './../components/Firebase/firebase'
export const ProductsUser2Context = createContext();

export class ProductsUser2ContextProvider extends React.Component {

    //defining an innitial state with empty array of products
    state = {
        products2: [],
        moved2: [],
        id: JSON.parse(localStorage.getItem('idRegion2'))
    }

    updateIdRegion() {
        var listIdRegion = this.state.moved2.find(x => x.idOrder >= 1)
        if (!!listIdRegion === true) {
            // localStorage.setItem('idRegion', listIdRegion.idOrder);
            if (listIdRegion.idOrder-2 === 1 || listIdRegion.idOrder === 36 || listIdRegion.idOrder === 95) {
                localStorage.setItem('idRegion2', listIdRegion.idOrder);
                window.location.reload()
            }
            // this.setState({id: listIdRegion.idOrder})
        }
    }
    componentDidMount() {
        const prevProducts = this.state.products2;
        this.setTime = setInterval(() => {
            movedService.getMoved2().then((response) => {
              this.setState({ moved2: response.data });
            });
            this.updateIdRegion()
          }, 2000)
            console.log(this.state.id)
        dbstore.collection('Products').where('IdRegion', '<', this.state.id)
        .onSnapshot(snapshot => {
            let changes = snapshot.docChanges();
            changes.forEach(change => {
                if (change.type === 'added') {
                    prevProducts.push({
                        ProductID: change.doc.id,
                        IdRegion: change.doc.data().IdRegion,
                        NameRegion: change.doc.data().NameRegion,
                        ProductName: change.doc.data().ProductName,
                        ProductPrice: change.doc.data().ProductPrice,
                        ProductImg: change.doc.data().ProductImg,
                    })
                }
                this.setState({
                    products2: prevProducts
                })
            })
        })
        dbstore.collection('Products').where('IdRegion', '==', 100)
        .onSnapshot(snapshot => {
            let changes = snapshot.docChanges();
            changes.forEach(change => {
                if (change.type === 'added') {
                    prevProducts.push({
                        ProductID: change.doc.id,
                        IdRegion: change.doc.data().IdRegion,
                        NameRegion: change.doc.data().NameRegion,
                        ProductName: change.doc.data().ProductName,
                        ProductPrice: change.doc.data().ProductPrice,
                        ProductImg: change.doc.data().ProductImg,
                    })
                }
                this.setState({
                    products2: prevProducts
                })
            })
        })

    }
    componentWillUnmount() {
        clearInterval(this.setTime);
    }
    render() {
        return (
            <ProductsUser2Context.Provider value={{ products2: [...this.state.products2] }}>
                {this.props.children}
            </ProductsUser2Context.Provider>
        )
    }
}