
import React, { createContext } from 'react';
import { db } from './../components/Firebase/firebase'
export const LocalNews2Context = createContext();

export class LocalNews2ContextProvider extends React.Component {

    state = {
        news2: [],
        id: JSON.parse(localStorage.getItem('idRegion2'))
    }

    componentDidMount() {
        const newState = this.state.news2;
            console.log(this.state.id)
            db.ref("list-news").once('value', (res) => {
                res.forEach((change) => {
                    if(change.val().idRegion< this.state.id){
                    newState.push({
                        idRegion: change.val().idRegion,
                        nameRegion: change.val().nameRegion,
                        title: change.val().title,
                        link: change.val().link,
                        img: change.val().img,
                        description: change.val().description
                    })
                }
                })
                this.setState({
                    news2: newState
                })
            });

    }
    componentWillUnmount() {
        clearInterval(this.setTime);
    }
    render() {
        return (
            <LocalNews2Context.Provider value={{ news2: [...this.state.news2] }}>
                {this.props.children}
            </LocalNews2Context.Provider>
        )
    }
}