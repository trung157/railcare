
import React, { createContext } from 'react';
import { db } from './../components/Firebase/firebase'
export const LocalNewsContext = createContext();

export class LocalNewsContextProvider extends React.Component {

    state = {
        news: [],
        id: JSON.parse(localStorage.getItem('idRegion'))
    }

    componentDidMount() {
        const newState = this.state.news;
            console.log(this.state.id)
            db.ref("list-news").once('value', (res) => {
                res.forEach((change) => {
                    if(change.val().idRegion> this.state.id){
                    newState.push({
                        idRegion: change.val().idRegion,
                        nameRegion: change.val().nameRegion,
                        title: change.val().title,
                        link: change.val().link,
                        img: change.val().img,
                        description: change.val().description
                    })
                }
                })
                this.setState({
                    news: newState
                })
            });

    }
    componentWillUnmount() {
        clearInterval(this.setTime);
    }
    render() {
        return (
            <LocalNewsContext.Provider value={{ news: [...this.state.news] }}>
                {this.props.children}
            </LocalNewsContext.Provider>
        )
    }
}