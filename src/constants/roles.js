export const ADMIN = 'ADMIN';
export const USER = 'USER';
export const STAFF_INSPECTOR = 'STAFF_INSPECTOR';
export const STAFF_ORDER = 'STAFF_ORDER';
export const STAFF_GUARD = 'STAFF_GUARD';